package com.gfi.dao;

import com.gfi.model.Repertoire;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RepertoireRepository extends JpaRepository<Repertoire, Long> {


    Optional<Repertoire> findByCodeRepertoire(String codeRepertoire);

    @Query("select r.id from Repertoire r")
    List<Long> getAllIds();

    Optional<Repertoire> findByNomRepertoire(String nomRepertoire);
}
