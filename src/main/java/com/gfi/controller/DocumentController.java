package com.gfi.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gfi.constant.ApplicationConfig;
import com.gfi.constant.FileUtil;
import com.gfi.controller.feigns.IFeignGetQuestion;
import com.gfi.dao.DocumentRepository;
import com.gfi.dao.RoleRepository;
import com.gfi.dao.UserRepository;
import com.gfi.dto.DocumentDTO;
import com.gfi.dto.Question;

import com.gfi.dto.Response.DeplacerDocumentResponceDTO;

import com.gfi.mapper.DocumentMapper;
import com.gfi.mapper.RepertoireMapper;
import com.gfi.model.*;
import com.gfi.service.Impl.DocumentServiceImpl;
import com.gfi.service.Impl.RepertoireServiceImpl;


import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;

import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartRequest;


import java.io.IOException;
import java.util.*;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("document")
public class DocumentController {
    @Autowired
    DocumentServiceImpl documentService;

    @Autowired
    RepertoireServiceImpl repertoireService;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private DocumentMapper documentMapper;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RepertoireMapper repertoireMapper;

    @Autowired
    private LoadBalancerClient loadBalancer;

    @Autowired
    private IFeignGetQuestion iFeignGetQuestion;

    List<Repertoire> repertoires = new ArrayList<>();

    List<String> files = new ArrayList<String>();


    @PostMapping("/{id_user}/post")
    public ResponseEntity<String> handleFileUpload(@RequestParam("file") MultipartFile file, String codeRepertoire, @PathVariable Long id_user) {


        String message = "";
        try {

            documentService.UploadDocument(file, codeRepertoire, id_user);
            files.add(file.getOriginalFilename());

            message = "You successfully uploaded " + file.getOriginalFilename() + "!";
            return ResponseEntity.status(HttpStatus.OK).body(message);
        } catch (Exception e) {
            message = "FAIL to upload " + file.getOriginalFilename() + "!";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(message);
        }
    }

    @PostMapping(value = "/filesToRepertoire")
    public ResponseEntity<String> handleFilesToRepertoire(@RequestParam String codeRepertoire,
                                                          @RequestParam("file") MultipartFile[] uploadfiles,
                                                          MultipartRequest request) {
        String message = "";
        Collection<MultipartFile> multipartFiles = request.getFiles("file");
        try {

            documentService.uploadFiles(multipartFiles, codeRepertoire);

            for (MultipartFile mf : multipartFiles) {

                message = "You successfully uploaded " + mf.getOriginalFilename() + "!";

            }

            return ResponseEntity.status(HttpStatus.OK).body("succes");
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body("Failed");
        }

    }

    @GetMapping("/{id_user}/get")
    public List<Document> getAllDocument(@PathVariable Long id_user) {
        try {
            return this.documentService.getAllDocument(id_user);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    @DeleteMapping(path = "/delete/{id}")
    public ResponseEntity delete(@PathVariable("id") Long id) {
        try {
            documentService.DeleteDocument(id);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body("Failed");
        }

        return ResponseEntity.status(HttpStatus.ACCEPTED).build();
    }

    @PostMapping("/deplaceDocument/{id}")
    public ResponseEntity<DeplacerDocumentResponceDTO> deplaceDocument(@PathVariable(value = "id") Long id,
                                                                       @RequestParam String codeRepertoire) {
        DeplacerDocumentResponceDTO deplacerDocumentResponceDTO = new DeplacerDocumentResponceDTO();

        try {
            Document document = this.documentService.deplacerDocument(id, codeRepertoire);
            deplacerDocumentResponceDTO.setDocument(document);
            deplacerDocumentResponceDTO.setSucces(1);
            deplacerDocumentResponceDTO.setMessage("operation reussite");
            return new ResponseEntity<DeplacerDocumentResponceDTO>(deplacerDocumentResponceDTO, HttpStatus.OK);
        } catch (Exception e) {

            deplacerDocumentResponceDTO.setSucces(0);
            deplacerDocumentResponceDTO.setMessage("failed : " + e.getMessage());
            return new ResponseEntity<DeplacerDocumentResponceDTO>(deplacerDocumentResponceDTO, HttpStatus.FORBIDDEN);
        }


    }

    @PutMapping("/edite/{id}")
    public ResponseEntity<Boolean> update(@PathVariable Long id, @RequestBody String nomDoc

    ) {
        try {
            boolean result = false;

            result = documentService.EditeDocument(id, nomDoc);

            if (result)
                return new ResponseEntity<Boolean>(new Boolean(result), HttpStatus.ACCEPTED);
            else
                return new ResponseEntity<Boolean>(new Boolean(result), HttpStatus.NOT_ACCEPTABLE);
        } catch (Exception e) {
            return new ResponseEntity<Boolean>(false, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping(value = "/details/{id}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ResponseBody
    public byte[] getContentFile(@PathVariable Long id) {
        try {
            HttpHeaders headers = new HttpHeaders();
            byte fileContent[] = new byte[0];
            try {
                fileContent = documentService.detailDocument(id, headers);
            } catch (Exception e) {
                e.printStackTrace();
            }
            String content = new String(fileContent);
            //headers.add("Access-Control-Allow-Origin", "*");
            return fileContent;
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return null;
        }


    }

    @GetMapping(value = "/download/{id}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ResponseBody
    public ResponseEntity<?> downloadFile(@PathVariable Long id) {
        try {
            HttpHeaders headers = new HttpHeaders();
            byte fileContent[] = new byte[0];
            try {
                fileContent = documentService.detailDocument(id, headers);
            } catch (Exception e) {
                e.printStackTrace();
            }
            String content = new String(fileContent);
            //headers.add("Access-Control-Allow-Origin", "*");
            return new ResponseEntity<byte[]>(fileContent, headers, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity<String>(e.getMessage(), HttpStatus.NOT_FOUND);
        }


    }

    @GetMapping("/tutorials/{nomDoc}")
    public List<Document> getAllTutorials(@PathVariable String nomDoc) {


        return this.documentService.searchDocument(nomDoc);



    }


    @GetMapping(value = "/{id_user}/search/name", produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public List<Repertoire> findByName(
            @RequestParam("contains") String name, @PathVariable Long id_user) {
        try {
            System.out.println(repertoireService.searchByNameDoc(name, id_user));
            return repertoireService.searchByNameDoc(name, id_user);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @DeleteMapping(value = "/{codeRep}")
    public ResponseEntity<Boolean> deleteContentDirectory(@PathVariable("codeRep") String codeRep) {

        try {
            boolean response = this.documentService.deleteContentDirectory(codeRep);
            return new ResponseEntity<Boolean>(response, HttpStatus.OK);
        } catch (IOException e) {
            return new ResponseEntity<Boolean>(false, HttpStatus.NOT_FOUND);
        }

    }

    @RequestMapping(value = "/Object/getListfeign", method = RequestMethod.GET)
    @HystrixCommand(fallbackMethod = "getDataFallBack")
    public  ResponseEntity<?> getAllQuestionfeign(){


        List<Question> responseEntity=new ArrayList<>();
        responseEntity=iFeignGetQuestion.getAllQuestionwithfein();

        return new ResponseEntity<>(responseEntity,HttpStatus.OK);
    }

    @RequestMapping(value = "/Object/getList", method = RequestMethod.GET)
    @HystrixCommand(fallbackMethod = "getDataFallBack")
    public  ResponseEntity<?> getAllQuestion(){
        ServiceInstance serviceInstance=loadBalancer.choose("ged-zuul-service");
        String baseUrl=serviceInstance.getUri().toString();
        System.out.println(baseUrl);
        final String uri = baseUrl+"/faqservice/api/question";

        RestTemplate restTemplate = new RestTemplate();
        List<Question> responseEntity=new ArrayList<>();

            responseEntity = restTemplate.getForObject(uri, List.class);


        return new ResponseEntity<>(responseEntity,HttpStatus.OK);
    }
    public ResponseEntity<?> getDataFallBack(){
        return new ResponseEntity<>("problem in service faq",HttpStatus.SERVICE_UNAVAILABLE);
    }

    @RequestMapping(value = "/Object/post/{id_user}", method = RequestMethod.POST)
    public Question createQuestion(@RequestBody Question question, @PathVariable Long id_user)throws RestClientException {

        ServiceInstance serviceInstance=loadBalancer.choose("faqservice");
        String baseUrl=serviceInstance.getUri().toString();
        System.out.println(baseUrl);
        final String uri = baseUrl+"/api/question/post";
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        Optional<User> user = this.userRepository.findById(id_user);

        Role role=roleRepository.findByName(RoleName.ROLE_ADMIN).get();
        if (user.get().getRoles().contains(role)) {
            RestTemplate restTemplate = new RestTemplate();
            question.setUser(user.get());
            Question responseEntity = restTemplate.postForObject(uri, question, Question.class);
            return responseEntity;
        }
        return null;
    }
    @RequestMapping(value = "/Object/delete/{id_user}", method = RequestMethod.DELETE)
    public ResponseEntity<Boolean> deleteQuestion(@PathVariable Long id_user){
        final String uri = "http://localhost:8089/api/question/"+id_user;
        RestTemplate restTemplate = new RestTemplate();

         restTemplate.delete(uri, id_user);
        return new ResponseEntity<Boolean>(true, HttpStatus.OK);
    }
    @RequestMapping(value = "/Object/update/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Question> updateQuestion(@RequestBody Question question, @PathVariable Long id){
        final String uri = "http://localhost:8089/api/question/"+id;
        RestTemplate restTemplate = new RestTemplate();

        restTemplate.put(uri,question, id);
        return new ResponseEntity<Question>(question, HttpStatus.OK);
}
    @RequestMapping(value = "/Object/search", method = RequestMethod.GET)
    public List<Question> searchQuestion(@RequestParam("contains") String question) {
        final String uri = "http://localhost:8089/api/question/tutorials?contains="+question;

        RestTemplate restTemplate = new RestTemplate();
        List<Question> responseEntity = restTemplate.getForObject(uri, List.class);
        return responseEntity;
    }
}


