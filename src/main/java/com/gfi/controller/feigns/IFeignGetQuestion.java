package com.gfi.controller.feigns;


import com.gfi.dto.Question;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;
@FeignClient(name="ged-zuul-service")
public interface IFeignGetQuestion {
    @GetMapping("/faqservice/api/question/withfein")
    public List<Question> getAllQuestionwithfein();

}
