package com.gfi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gfi.dto.RepertoireDTO;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import java.util.*;

@Entity
@Data
@Slf4j
public class Document {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    private Long id;


    private String nomDoc;


    private Long taille;


    private String description;

    @Temporal(TemporalType.DATE)
    private Date dateCreation;


    private String type;
    @JsonIgnore
    @ManyToOne
    public Repertoire repertoire;


    public Long getId() {
        return id;
    }

    public String getNomDoc() {
        return nomDoc;
    }

    public Long getTaille() {
        return taille;
    }

    public String getDescription() {
        return description;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public String getType() {
        return type;
    }


    public void setId(Long id) {
        this.id = id;

    }

    public void setNomDoc(String nomDoc) {
        this.nomDoc = nomDoc;
    }

    public void setTaille(Long taille) {
        this.taille = taille;
        System.out.println(taille + "octets");
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setRepertoire(Repertoire repertoire) {
        this.repertoire = repertoire;
    }

    public Repertoire getRepertoire() {
        return repertoire;
    }

    public Document(Long id, String nomDoc, Long taille, String description, Date dateCreation, String type, Repertoire repertoire) {
        this.id = id;
        this.nomDoc = nomDoc;
        this.taille = taille;
        this.description = description;
        this.dateCreation = dateCreation;
        this.type = type;
        this.repertoire = repertoire;
    }

    public Document(Long id, Long taille, Date dateCreation, Repertoire repertoire) {
        this.id = id;
        this.taille = taille;
        this.dateCreation = dateCreation;
        this.repertoire = repertoire;

    }

    public Document() {
    }

    public Document(String nomDoc, String description, String type) {
        this.nomDoc = nomDoc;
        this.description = description;
        this.type = type;
    }

    @Override
    public String toString() {
        return "Document{" +
                "id=" + id +
                ", nomDoc='" + nomDoc + '\'' +
                ", taille=" + taille +
                ", description='" + description + '\'' +
                ", dateCreation=" + dateCreation +
                ", type='" + type + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Document document = (Document) o;
        return id.equals(document.id) &&
                Objects.equals(nomDoc, document.nomDoc) &&
                Objects.equals(taille, document.taille) &&
                Objects.equals(description, document.description) &&
                Objects.equals(dateCreation, document.dateCreation) &&
                Objects.equals(type, document.type) &&
                Objects.equals(repertoire, document.repertoire);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nomDoc, taille, description, dateCreation, type, repertoire);
    }
}
