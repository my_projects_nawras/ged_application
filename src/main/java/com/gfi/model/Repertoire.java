package com.gfi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.*;

@Entity


public class Repertoire {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    private Long id;


    private String nomRepertoire;

    @Column(unique = true)
    private String codeRepertoire;

    @Temporal(TemporalType.DATE)
    private Date dateCreation;

    @OneToMany(mappedBy = "repertoire", orphanRemoval = true, cascade = CascadeType.REMOVE)
    private Set<Document> documents = new HashSet<>();

    public Set<Document> getDocuments() {
        return documents;
    }

    @JsonIgnore
    @ManyToOne()
    public User user;


    @Override
    public String toString() {
        return "Repertoire{" +
                "id=" + id +
                ", nomRepertoire='" + nomRepertoire + '\'' +
                ", codeRepertoire='" + codeRepertoire + '\'' +
                ", dateCreation=" + dateCreation +
                ", documents=" + documents +
                '}';
    }

    public void setDocuments(Set<Document> documents) {
        this.documents = documents;
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public String getNomRepertoire() {
        return nomRepertoire;
    }

    public String getCodeRepertoire() {
        return codeRepertoire;
    }

    public Date getDateCreation() {
        return dateCreation;

    }


    public void setId(Long id) {
        this.id = id;
    }

    public String setNomRepertoire(String nomRepertoire) {
        return this.nomRepertoire = nomRepertoire;
    }

    public void setCodeRepertoire(String codeRepertoire) {
        this.codeRepertoire = codeRepertoire;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }


    public String formatCodeRepertoire(Long id) {
        String formatter = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());


        return formatter +
                nomRepertoire + "-" +
                id;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Repertoire that = (Repertoire) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }


}
