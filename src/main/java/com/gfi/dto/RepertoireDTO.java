package com.gfi.dto;

import lombok.Data;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.List;

@Data
public class RepertoireDTO {

    private Long id;

    private String nomRepertoire;

    private String codeRepertoire;

    private Date dateCreation;


    public Long getId() {
        return id;
    }

    public String getNomRepertoire() {
        return nomRepertoire;
    }

    public String getCodeRepertoire() {
        return codeRepertoire;
    }

    public Date getDateCreation() {

        return dateCreation;
    }


    public void setId(Long id) {
        this.id = id;
    }

    public String setNomRepertoire(String nomRepertoire) {
        return this.nomRepertoire = nomRepertoire;
    }

    public void setCodeRepertoire(String codeRepertoire) {
        this.codeRepertoire = codeRepertoire;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }


    public RepertoireDTO(Long id, String nomRepertoire, String codeRepertoire, Date dateCreation) {
        this.id = id;
        this.nomRepertoire = nomRepertoire;
        this.codeRepertoire = codeRepertoire;
        this.dateCreation = dateCreation;
    }

    public RepertoireDTO() {
    }
}
