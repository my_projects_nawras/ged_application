package com.gfi.dto.Response;

import com.gfi.model.Repertoire;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AllRepertoireResponseDTO extends BaseResponseDTO {

    Set<Repertoire> repertoires = new HashSet<>();

    public AllRepertoireResponseDTO(int succes, String message, Set<Repertoire> repertoires) {
        super(succes, message);
        this.repertoires = repertoires;
    }

    public AllRepertoireResponseDTO() {
    }

    public AllRepertoireResponseDTO(Set<Repertoire> repertoires) {
        this.repertoires = repertoires;
    }

    public Set<Repertoire> getRepertoires() {
        return repertoires;
    }

    public void setRepertoires(Set<Repertoire> repertoires) {
        this.repertoires = repertoires;
    }


}
