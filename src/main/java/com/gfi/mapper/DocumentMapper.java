package com.gfi.mapper;

import com.gfi.dto.DocumentDTO;
import com.gfi.model.Document;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.Map;

@Mapper(uses = {RepertoireMapper.class}, componentModel = "spring")
public interface DocumentMapper {

    //DocumentMapper INSTANCES = Mappers.getMapper( DocumentMapper.class );

    DocumentDTO DocumentDTO(Document document);

    //DocumentDTO documentDTO=new DocumentDTO();
    Document DocumentDTOToDocument(DocumentDTO dto);
}
