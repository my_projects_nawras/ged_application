package com.gfi.service;

import com.gfi.dto.RepertoireDTO;
import com.gfi.model.Repertoire;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface RepertoireService {
    boolean createDirectory(String nom, Long id_user) throws IOException, ParseException;

    Optional<Repertoire> findByID(Long id);

    boolean EditeDirectory(Repertoire Repertoire, Long idRep);

    void DeleteDirectory(Long id);

    List<Repertoire> findAll();

    List<Long> getRepertoireids();

    Set<Repertoire> getAllRepertoire(Long id_user) throws Exception;

    List<Repertoire> searchByNameDoc(String nomDoc, Long id_user) throws Exception;

}
