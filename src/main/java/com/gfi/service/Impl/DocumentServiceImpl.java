package com.gfi.service.Impl;

import com.gfi.constant.ApplicationConfig;
import com.gfi.dao.DocumentRepository;
import com.gfi.dao.RepertoireRepository;
import com.gfi.dao.UserRepository;
import com.gfi.dto.Question;
import com.gfi.exception.MyFileNotFoundException;

import com.gfi.model.Document;
import com.gfi.model.Repertoire;
import com.gfi.model.User;
import com.gfi.service.DocumentService;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class DocumentServiceImpl implements DocumentService {
    @Autowired
    DocumentRepository documentRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    RepertoireServiceImpl repertoireservice;

    @Autowired
    RepertoireRepository repertoireRepository;

    ApplicationConfig path;


    private final Path rootLocation = Paths.get(ApplicationConfig.PATH);

    @Override
    public void UploadDocument(MultipartFile file, String codeRepertoire, Long id_user) {

        try {
            Repertoire repertoire = repertoireRepository.findByCodeRepertoire(codeRepertoire).get();
            Optional<User> user = this.userRepository.findById(id_user);
            if (repertoire.getUser().equals(user.get())) {
                Path nom = this.rootLocation.resolve(codeRepertoire);
                Files.copy(file.getInputStream(), nom.resolve(file.getOriginalFilename()));
                Document document = new Document();
                document.setNomDoc(file.getOriginalFilename());
                document.setTaille(file.getSize());
                document.setType(file.getContentType());
                document.setDateCreation(repertoire.getDateCreation());
                document.setRepertoire(repertoire);
                System.out.println(document.getRepertoire().getUser());
                this.documentRepository.save(document);
            } else {
                throw new Exception("vous n'avez pas le droit d'ajouter un document à cette repertoire");
            }

        } catch (Exception e) {
            throw new RuntimeException("FAIL!");
        }
    }

    @Override
    public void init() {
        try {
            Files.createDirectory(rootLocation);
        } catch (IOException e) {
            throw new RuntimeException("Could not initialize storage!");
        }
    }

    @Override
    public List<Document> getAllDocument(Long id_user) throws Exception {
        Optional<User> user = this.userRepository.findById(id_user);
        List<Document> documents = new ArrayList<>();
        if (user.isPresent()) {
            Set<Repertoire> repertoires = user.get().getRepertoires();
            for (Repertoire repertoire1 : repertoires) {
                documents.addAll(repertoire1.getDocuments());
            }
        }
        return documents;

//        Optional<Repertoire> repertoire= this.repertoireRepository.findById(id_rep);
//        Set<Document> document =  repertoire.get().getDocuments();
//        Optional<User> users= this.userRepository.findById(id_user);
//        if(repertoire.get().getUser().equals(users.get())) {
//
//            return document;
//    }
//        else

    }


    @Override
    public Document getFile(Long fileId) {
        return documentRepository.findById(fileId)
                .orElseThrow(() -> new MyFileNotFoundException("File not found with id " + fileId));
    }

    @Override
    public boolean DeleteDocument(Long id) throws Exception {

        Document document = documentRepository.findById(id).get();
//        Optional<User> user=this.userRepository.findById(id_user);

        if (document != null) {
//            if(document.getRepertoire().getUser().equals(user.get())) {
            Path pathSource = this.rootLocation.resolve(document.getRepertoire().getCodeRepertoire() + "\\" + document.getNomDoc());
            File file = pathSource.toFile();
            if (file.delete()) {
                this.documentRepository.deleteById(id);
                return true;
            }

//            }else {
//                throw new Exception("vous n'avez pas le droit de supprimer document ");
//            }
        }
        return false;
    }

    @Override
    public void uploadFiles(Collection<MultipartFile> multipartFile, String codeRepertoire) throws IOException, Exception {
        {

            Path nom = this.rootLocation.resolve(codeRepertoire);
            for (MultipartFile mf : multipartFile) {
                if (mf.isEmpty()) {
                    continue;//next pls
                }
                byte[] bytes = mf.getBytes();
                Path path = Paths.get(ApplicationConfig.TEMP_DIR + "\\" + codeRepertoire + "\\" + mf.getOriginalFilename());
                Document document = new Document();

                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                Date date = new Date(System.currentTimeMillis());

                Repertoire repertoire = repertoireRepository.findByCodeRepertoire(codeRepertoire).get();

                Files.write(path, bytes);
                document.setNomDoc(mf.getOriginalFilename());
                document.setTaille(mf.getSize());
                document.setType(mf.getContentType());
                document.setDateCreation(date);
                document.setDescription(mf.getResource().toString());
                document.setRepertoire(repertoire);
                this.documentRepository.save(document);

            }


        }
    }

    @Override
    public Document deplacerDocument(Long id, String codeRepertoire) throws Exception {

        Document document = documentRepository.findById(id).get();
//        Optional<User> user=this.userRepository.findById(id_user);

        Path destination = this.rootLocation.resolve(codeRepertoire);
        Path source = this.rootLocation.resolve(document.getRepertoire().getCodeRepertoire());
        File afileSource = new File(source + "\\" + document.getNomDoc());
        File afileDestination = new File(destination + "\\" + document.getNomDoc());
        boolean sourceboolean = afileSource.exists();
        boolean sodistinationboolean = afileDestination.exists();
//        if(document.getRepertoire().getUser().equals(user.get())) {
        if (afileSource.exists() && (!afileDestination.exists())) {
            if (afileSource.renameTo(afileDestination)) {
                System.out.println("File is moved successful!");
            } else {
                System.out.println("File is failed to move!");
            }
        } else if (!afileSource.exists()) {
            throw new Exception("File source not exist");
        } else if (afileDestination.exists()) {
            throw new Exception("File destination  exist");
        } else throw new Exception("une erreur inattendu");


        afileSource.delete();
        document.setRepertoire(repertoireRepository.findByCodeRepertoire(codeRepertoire).get());
        this.documentRepository.save(document);

//        else {
//                throw new Exception("vous n'avez pas le droit de deplacer document ");
//            }
        return document;
    }

    @Override
    public boolean EditeDocument(Long id, String nomDoc) throws Exception {
        Repertoire repertoire = new Repertoire();
        Document document = this.documentRepository.findById(id).get();
//      Optional<User> user=this.userRepository.findById(id_user);


        // Repertoire repertoire = repertoireRepository.findById(id).get();

        Path pathSource = this.rootLocation.resolve(document.getRepertoire().getCodeRepertoire() + "\\" + document.getNomDoc());

        Path pathDestination = this.rootLocation.resolve(document.getRepertoire().getCodeRepertoire() + "\\" + nomDoc);

        File fileSource = pathSource.toFile();
        File fileDestination = pathDestination.toFile();
//        if(document.getRepertoire().getUser().equals(user.get())) {
        if (fileSource.renameTo(fileDestination)) {
            document.setNomDoc(nomDoc);

            this.documentRepository.save(document);
            return true;
        } else {
            return false;
        }
//        else {
//            throw new Exception("vous n'avez pas le droit de deplacer document ");
//        }

    }

    @Override
    public byte[] detailDocument(Long id, HttpHeaders headers) throws Exception {
        Document document = this.documentRepository.findById(id).get();
//        Optional<User> user=this.userRepository.findById(id_user);

        Path path = this.rootLocation.resolve(document.getRepertoire().getCodeRepertoire() + "\\" + document.getNomDoc());
        File pdfFile = path.toFile();
//        if(document.getRepertoire().getUser().equals(user.get())) {
        headers.setContentType(MediaType.parseMediaType(document.getType()));
        headers.add("Content-Length", pdfFile.length() + "");
        headers.add("Content-Type", document.getType());
        headers.add("Content-Disposition", "attachment; filename=" + document.getNomDoc());
        FileInputStream fileInputStream = new FileInputStream(pdfFile);
        byte[] fileContent = new byte[(int) pdfFile.length()];
        fileInputStream.read(fileContent);
        return fileContent;

    }

    @Override
    public List<Document> searchDocument(String nomDoc) {
        return documentRepository.findByNomDoc(nomDoc);
    }

    @Override
    public boolean deleteContentDirectory(String codeRep) throws IOException {
        Optional<Repertoire> repertoire = this.repertoireRepository.findByCodeRepertoire(codeRep);
        if (repertoire.isPresent()) {
            Path path = this.rootLocation.resolve(repertoire.get().getCodeRepertoire());
            FileUtils.deleteDirectory(path.toFile());
            repertoireRepository.delete(repertoire.get());
            return true;
        }

        return false;
    }


}
