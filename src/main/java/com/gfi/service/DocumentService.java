package com.gfi.service;

import com.gfi.dto.Question;
import com.gfi.model.Document;
import org.springframework.http.HttpHeaders;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface DocumentService {
    void UploadDocument(MultipartFile file, String codeRepertoire, Long id_user) throws IOException;

    Document getFile(Long fileId);

    void init();

    List<Document> getAllDocument(Long id_user) throws Exception;

    boolean DeleteDocument(Long id) throws Exception;

    void uploadFiles(Collection<MultipartFile> multipartFile, String codeRepertoire) throws IOException, Exception;

    Document deplacerDocument(Long id, String codeRepertoire) throws Exception;

    boolean EditeDocument(Long id, String nomDoc) throws Exception;

    byte[] detailDocument(Long id, HttpHeaders headers) throws Exception;

    List<Document> searchDocument(String nomDoc);

    boolean deleteContentDirectory(String codeRep) throws IOException;


}
