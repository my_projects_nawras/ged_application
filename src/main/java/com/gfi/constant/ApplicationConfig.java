package com.gfi.constant;

public class ApplicationConfig {
    public static final String TEMP_DIR = "C:\\dossier";
    public static final String TEMP = "C:\\dossier\\Javanawras3";
    public static final String PATH = "C:\\dossier\\";

    public static final String PNG_FILE_FORMAT = ".png";
    public static final String JPEG_FILE_FORMAT = ".jpeg";
    public static final String JPG_FILE_FORMAT = ".jpg";

    private ApplicationConfig() {
    }
}
